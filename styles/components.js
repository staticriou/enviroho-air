import React from "react";
import {
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Text,
} from "react-native";
import { center } from "./basics";

const styles = StyleSheet.create({
  button: {
    alignItems: "center",
    backgroundColor: "#fff",
    borderRadius: 10,
    paddingVertical: 10,
  },
  textButton: {
    color: "#1F73B0",
    fontWeight: "bold",
    fontSize: 18,
    textAlign: "center",
  },
    labelSign: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    textAlign: "center",
  },
});

export const Input = (props) => {
  return <TextInput {...props} maxLength={40} editable />;
};

export const Klik = ({ text, klik }) => {
  return (
    <TouchableOpacity style={styles.button} onPress={klik}>
      <Text style={styles.textButton}>{text}</Text>
    </TouchableOpacity>
  );
};

export const Gap = () => {
  return <View style={{ paddingBottom: 20 }} />;
};

export const Skip = ({navigation}) => {
  return (
    <View style={{bottom: 0, right: 0, position: "absolute", padding: 10}}>
      <TouchableOpacity onPress={() => navigation.navigate('Home')}>
        <Text style={styles.labelSign}>Skip >></Text>
      </TouchableOpacity>
    </View>
    );
};