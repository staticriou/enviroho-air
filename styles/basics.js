export const container = {
  flex: 1,
};

export const center = {
  justifyContent: "center",
  alignItems: "center",
};

export const column = {
  flexDirection: "column",
};

export const row = {
  flexDirection: "row",
};

export const border = {
  borderWidth: 1,
  borderStyle: "solid",
};

export const borderRound = {
  ...border,
  ...column,
  justifyContent: "center",
};

export const borderTop = {
  borderTopWidth: 1,
  borderStyle: "solid",
  padding: 10,
};

export const bar = {
  height: 55,
  elevation: 3,
  paddingHorizontal: 15,
  paddingVertical: 10,
  flexDirection: "row",
  alignItems: "center",
  justifyContent: "space-between",
};
