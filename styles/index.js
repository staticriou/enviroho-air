import * as Basic from "./basics";
import * as Color from "./colors";
import * as Typo from "./typos";

export { Basic, Color, Typo };
