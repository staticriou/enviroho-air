import { colorFont } from "./colors";

export const headline = {
  fontSize: 30,
  fontWeight: "bold",
  color: colorFont,
  textAlign: "center",
};

export const h1 = {
  fontSize: 32,
  fontWeight: "bold",
};

export const h2 = {
  fontSize: 24,
};

export const h3 = {
  fontSize: 18,
};

export const label = {
  fontSize: 13,
  color: colorFont,
};
