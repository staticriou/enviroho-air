import React, { useEffect, useState } from "react";
import {
  Platform,
  ActivityIndicator,
  FlatList,
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
} from "react-native";
// import { StackedBarChart } from 'react-native-svg-charts'
import { color } from "react-native-reanimated";
import { Typo, Basic, Color } from "../styles";
import { Gap } from "../styles/components";

const Detail = ({ route, navigation }) => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const { cityname } = route.params;

  useEffect(() => {
    fetch(`https://api.openaq.org/v1/measurements?city=${cityname}`, {
      method: "GET",
    })
      .then((res) => res.json())
      // IF CONDITIONAL FOR ANY DIFFERENT PARAMETER
      .then((resJson) => {
        setData(resJson.results);
        // if (resJson.results[0].parameter !== resJson.results[1].parameter) {
        //   setData(resJson.results);
        // } else {
        //   setData(resJson.results[0]);
        // }
      })
      // .then((json) => setData(json.movies))
      .catch((err) => console.error(err))
      .finally(() => setLoading(false));
  });

  return (
    <View style={styles.container}>
      {isLoading ? (
        <ActivityIndicator />
      ) : (
        <View>
          <Text style={styles.headline}>{data[0].city}</Text>
                      <View
              style={{
                backgroundColor: "#000",
                alignContent: "stretch",
                paddingVertical: 3,
              }}
            >
              <Text style={styles.h3}>
                updated at {new Date().toLocaleDateString()}
              </Text>
            </View>
            <Gap/>
          <View style={styles.container2}>

            {/* PARAMETER 0*/}
            <View style={styles.boxRound}>
              <Text style={[styles.h3, {margin: 8}]}>
                Location: {data[0].location}, {data[0].city}, {data[0].country}
              </Text>
              <View style={styles.borderTop}>
                <Text style={[styles.h3b, {textAlign: "left"}]}>
                  Parameter: {data[0].parameter}
                </Text>
                <Text style={[styles.h3b, { textAlign: "left" }]}>
                  Concentration: {data[0].value} {data[0].unit}
                </Text>
              </View>
            </View>
          </View>
        </View>
      )}
      <View style={{bottom: 0, left: 0, position: "absolute", padding: 10}}>
        <TouchableOpacity style={styles.buttonRound} onPress={() => navigation.goBack()}>
          <Text style={styles.labelSign}>Back to Home</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    ...Basic.container,
  },
  container2: {
    ...Basic.container,
    paddingHorizontal: 10,
  },
  headline: {
    ...Typo.headline,
    backgroundColor: Color.colorBg,
  },
  h3: {
    ...Typo.h3,
    textAlign: "center",
    textDecorationLine: "underline",
    color: Color.colorFont,
  },
  h3b: {
    fontSize: 18,
  },
  boxRound: {
    ...Basic.borderRound,
    backgroundColor: Color.colorBg,
      borderTopLeftRadius: 10,
  borderTopRightRadius: 10,
  },
  borderTop: {
    ...Basic.borderTop,
    backgroundColor: Color.colorFont,
  },
  labelSign: {
    fontSize: 18,
    color: Color.colorFont,
    fontWeight: "bold",
    textAlign: "center",
  },
  buttonRound: {
    ...Basic.borderRound,
    backgroundColor: Color.colorBg,
    borderRadius: 30,
    borderColor: "#fff",
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
});

export default Detail;
