import React from "react";
import {
  Platform,
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
} from "react-native";
import { Input, Gap, Klik, Skip } from "../styles/components";
import { Typo, Basic, Color } from "../styles";

const Register = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Image
        source={require("../images/logo.png")}
        style={{ width: 80, maxHeight: 70, alignSelf: "center" }}
      />
      <Text style={styles.headline}>SIGN UP</Text>
      <Gap />
      <Gap />
      <View>
        <Text style={styles.label}>Email</Text>
        <Input  style={styles.input}/>
        <Gap />
        <Text style={styles.label}>Password</Text>
        <Input secureTextEntry={true}  style={styles.input}/>
        <Gap />
        <Text style={styles.label}>Confirm Password</Text>
        <Input secureTextEntry={true}  style={styles.input}/>
        <Gap />
        <Text style={styles.label}>Your location</Text>
        {/* <Input secureTextEntry={true} /> */}
        <Gap />
        <View>
          <Klik text="Sign Up" klik={() => alert("check")} />
          <Gap />
          <Text style={styles.labelCenter}>Already have an account?</Text>
          <TouchableOpacity onPress={() => navigation.navigate()}>
            <Text style={styles.labelSign}>Sign In</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={{bottom: 0, right: 0, position: "absolute", padding: 10}}>
        <TouchableOpacity style={styles.buttonRound} onPress={() => navigation.navigate('Home')}>
          <Text style={styles.labelSign}>Skip</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    ...Basic.container,
    backgroundColor: Color.colorBg,
    paddingHorizontal: 10,
    paddingTop: 20,
  },
  headline: {
    ...Typo.headline,
  },
  label: {
    ...Typo.label,
  },
  labelCenter: {
    ...Typo.label,
    textAlign: "center",
  },
  labelSign: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    textAlign: "center",
  },
    buttonRound: {
    ...Basic.borderRound,
    backgroundColor: Color.colorBg,
    borderRadius: 30,
    borderColor: "#fff",
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
    input: {
    borderColor: "#fff",
    borderBottomWidth: 1,
    borderStyle: "solid",
    paddingLeft: 5,
    fontSize: 18,
    color: "#fff",
    height: 30,
  },
});

export default Register;
