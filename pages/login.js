import React, {useState, useContext, useHistory} from "react";
import {
  Platform,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
} from "react-native";
import { Input, Gap, Klik, Skip } from "../styles/components";
import { Typo, Basic, Color } from "../styles";
// import {LoginContext} from "../context/loginprovider"


const Login = ({ navigation }) => {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [isButtonDisabled, setIsButtonDisabled] = useState(true)
  const [helperText, setHelperText] = useState('')
  const [error, setError] = useState(false)
  // const [status, setStatus] = useContext(LoginContext)
  // const history = useHistory()

  const handleLogin = () => {
    if (username === 'riyooho@xyz.com' && password === '123456') {
      setError(false)
      // setStatus(true)
      // setHelperText('Login Successfully')
      navigation.navigate("Tab");

    } else {
      setError(true)
      // setStatus(false)
      // setHelperText('Incorrect username or password')
    }
  }

  return (
    <View style={styles.container}>
      <Image
        source={require("../images/logo.png")}
        style={{ width: 80, maxHeight: 70, alignSelf: "center" }}
      />
      <Text style={styles.headline}>SIGN IN</Text>
      <Gap />
      <View>
        <Text style={styles.label}>Email</Text>
        <Input  style={styles.input} placeholder="riyooho@xyz.com" onChange={(e)=>setUsername(e.target.value)}/>
        <Gap />
        <Text style={styles.label}>Password</Text>
        <Input secureTextEntry={true}  style={styles.input} placeholder="123456" onChange={(e)=>setPassword(e.target.value)}/>
        <Gap />
        <View>
          <Klik text="Sign In" klik={() => handleLogin()} />
          <Gap />
          <Text style={styles.labelCenter}>Don't have an account?</Text>
          <TouchableOpacity onPress={() => navigation.navigate('Register')}>
            <Text style={styles.labelSign}>Sign Up</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={{bottom: 0, right: 0, position: "absolute", padding: 10}}>
        <TouchableOpacity style={styles.buttonRound} onPress={() => navigation.navigate('Tab')}>
          <Text style={styles.labelSign}>Skip</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    ...Basic.container,
    backgroundColor: Color.colorBg,
    paddingHorizontal: 10,
    paddingTop: 20,
  },
  headline: {
    ...Typo.headline,
  },
  label: {
    ...Typo.label,
  },
  labelCenter: {
    ...Typo.label,
    textAlign: "center",
  },
  labelSign: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    textAlign: "center",
  },
  buttonRound: {
    ...Basic.borderRound,
    backgroundColor: Color.colorBg,
    borderRadius: 30,
    borderColor: "#fff",
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
    input: {
    borderColor: "#fff",
    borderBottomWidth: 1,
    borderStyle: "solid",
    paddingLeft: 5,
    fontSize: 18,
    color: "#fff",
    height: 30,
  },
});

export default Login;
