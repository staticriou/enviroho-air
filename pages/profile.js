import React from "react";
import { Platform, StyleSheet, View, Text, Image } from "react-native";
import { Typo, Basic, Color } from "../styles";

const Profile = ({ navigation }) => {
  return (
    <View style={styles.container}>
      {/* Pengandaian: jika belum terdaftar sebagai user munculkan link untuk
      login atau register */}
      <View style={styles.bar}>
        <Text style={styles.headline}>Profile</Text>
      </View>
      <View style={styles.center}>
        <Image source={require("../images/ava.jpg")} style={{height: 180, width: 180, borderRadius: 200}}/>
        <Text style={styles.h2}>riyooho</Text>
        <Text style={styles.h3}>Address: Adelaide</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    ...Basic.container,
  },
  bar: {
    ...Basic.bar,
    backgroundColor: Color.colorBg,
  },
  headline: {
    ...Typo.headline,
  },
  center: {
    ...Basic.center,
    flex: 1,
  },
  h2: {
    ...Typo.h2,
  },
  h3: {
    ...Typo.h3,
  },
});

export default Profile;
