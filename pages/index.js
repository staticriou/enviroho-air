import React, { useEffect, useState } from "react";
import { AntDesign } from '@expo/vector-icons'; 
import Splash from "./splash";
import Login from "./login";
import Register from "./regis";
import Home from "./home";
import Detail from "./detail";
import Profile from "./profile"

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";

class Tab extends React.Component {
  render() {
    const Tabs = createBottomTabNavigator();
  return (
    <Tabs.Navigator tabBarOptions={{
            activeTintColor: '#1F73B0',
            style: {borderTopWidth: 1, borderStyle: "solid", borderColor: "#000"}
        }}>
      <Tabs.Screen name="Home" component={Home} options={{
                    title: "Home",
                    tabBarIcon: (props) => (
                        <AntDesign name="home" size={24} color="black" data={props}/>
                    )
                }}/>
      <Tabs.Screen name="Profile" component={Profile} options={{
                    title: "Home",
                    tabBarIcon: (props) => (
      <AntDesign name="user" size={24} color="black" />)
                }}/>
    </Tabs.Navigator>
  )}
}

export default class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isLoading: true };
  }

  performTimeConsumingTask = async () => {
    return new Promise((resolve) =>
      setTimeout(() => {
        resolve("result");
      }, 2000)
    );
  };

  async componentDidMount() {
    const data = await this.performTimeConsumingTask();
    if (data !== null) {
      this.setState({ isLoading: false });
    }
  }

  render() {
    if (this.state.isLoading) {
      return <Splash />;
    }
    const Stack = createStackNavigator();

    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login"  screenOptions={{
    headerShown: false
  }}>
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="Register" component={Register} />
          <Stack.Screen name="Tab" component={Tab} />
          <Stack.Screen name="Detail" component={Detail}/>
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
