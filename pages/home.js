import React, { useEffect, useState } from 'react';
import {
  Platform,
  ActivityIndicator,
  FlatList,
  Text,
  TouchableOpacity,
  View,
  Image,
  StyleSheet,
  ScrollView,
  SafeAreaView,
  Button,
} from 'react-native';
import { Basic, Typo, Color } from '../styles';
import { center } from '../styles/basics';
import { Gap, Input, Klik } from '../styles/components';
import Profile from "./profile";

const Home = ({ navigation }) => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch('https://api.openaq.org/v1/cities?country=AU', {
      method: 'GET',
    })
      .then((response) => response.json())
      .then((responseJson) => {
        setData(responseJson.results);
      })
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  return (
    <View style={styles.container}>
      {/* Pengandaian: jika belum terdaftar sebagai user munculkan link untuk
      login atau register */}
      <Text style={styles.h3}>Choose your location:</Text>
      <Input placeholder="Search..." style={styles.search}/>
      <Gap />
      <View style={styles.container2}>
        {isLoading ? (
          <ActivityIndicator />
        ) : (
          <View>
            <FlatList
              data={data}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item }) => (
                <TouchableOpacity
                  keyExtractor
                  onPress={() => {navigation.navigate('Detail', {
                    cityname: item.city
                  })}}
                  style={{
                    alignContent: 'stretch',
                    padding: 10,
                    borderBottomWidth: 1,
                    borderStyle: 'solid',
                  }}>
                  <Text style={styles.h2}>{item.city}</Text>
                </TouchableOpacity>
              )}
            />
          </View>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    ...Basic.container,
    backgroundColor: Color.colorFont
  },
  container2: {
    ...Basic.container,
    paddingHorizontal: 10,
  },
  h2: {
    ...Typo.h2,
  },
  h3: {
    ...Typo.h3,
    textAlign: 'center',
    textDecorationLine: 'underline',
  },
    search: {
    borderColor: "#000",
    borderBottomWidth: 1,
    borderStyle: "solid",
    paddingLeft: 5,
    fontSize: 18,
    color: "#000",
    height: 30,
  },
});

export default Home;
