import React from "react";
import { Platform, StyleSheet, View, Image } from "react-native";
import { Basic, Color } from "../styles";

const Splash = () => {
  return (
    <View style={styles.splash}>
      <View style={styles.rectangle}>
        <Image
          source={require("../images/logo.png")}
          style={{ width: 150, maxHeight: 110 }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  splash: {
    ...Basic.container,
    ...Basic.column,
    ...Basic.center,
    backgroundColor: Color.colorBg,
  },
  rectangle: {
    ...Basic.center,
    ...Basic.border,
    borderColor: "#fff",
    padding: 15,
  },
});

export default Splash;
