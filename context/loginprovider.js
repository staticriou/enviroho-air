import React, {useState, createContext} from "react";

export const LoginContext = createContext();

const LoginProvider = ({ children }) => {
    const [status, setStatus] = useState(false)

    return (
      <LoginContext.Provider value={{status, setStatus}}>
        {children}
      </LoginContext.Provider>
    )
  }
  
export default LoginProvider